import type { PageServerLoad } from './$types';
import { getPlayersListByTeam } from "$lib/game";

export const load = (({ cookies }) => {
    return {
        teams: getPlayersListByTeam()
    };
}) satisfies PageServerLoad;