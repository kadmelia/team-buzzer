type Player = {name: string, score: number};

type Team = {name: string, players: Player[]}; 

export function getPlayersListByTeam(): Team[] {
    return [
        {
            name: 'Poufsouffle',
            players: [
                {
                    name: 'Jean Mich',
                    score: 0,
                },
                {
                    name: 'Hector',
                    score: 20,
                },
            ]
        },
        {
            name: 'Serdaigle',
            players: [
                {
                    name: 'Maurice',
                    score: 30,
                },
                {
                    name: 'Plopman',
                    score: 5,
                },
            ]
        }

    ]
}